/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package valordevendafiliais;

import java.io.File;

/**
 *
 * @author Deive
 */
public class ValorDeVendaFiliais {

    public static void main(String[] args) {
        /*
        Uma loja possui 4 filiais, cada uma com um código de 1 a 4.
        Um arquivo contendo todas as vendas feitas durante o dia nas quatro filiais é gerado a
        partir de uma planilha, sendo que cada linha do arquivo contém o número da filial e o valor
        de uma venda efetuada, separados por vírgula.
        Ex.:
        1,189.90
        1,45.70
        3,29.90
        4,55.00

        No exemplo acima, a filial 1 fez duas vendas, a primeira
        totalizando R$ 189,90 e a segunda R$ 45,70. A filial 3 fez
        uma venda de R$ 29,90 e a 4 também uma de R$ 55,00.
        Faça um programa que leia este arquivo e informe, ao
        final, o total e o valor médio das vendas de cada filial.
        */
        
        String arquivoString = "src\\valordevendafiliais\\vendas.txt";
        
        SemVetor sv = new SemVetor(arquivoString);
        sv.GetVendas();
        
        /*
        File arquivo = new File("src\\valordevendafiliais\\vendas.txt");
        
        Utilitario utilitario = new Utilitario(arquivo);

        String[] vendas = utilitario.GetVendasPorFilial();
        for (String venda : vendas) {
            System.out.println(venda);
        }
        */
        
    }
    
}
