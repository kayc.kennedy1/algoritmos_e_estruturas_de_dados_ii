/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package valordevendafiliais;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.Arrays;

/**
 *
 * @author Deive
 */
public class Utilitario {
    
    private final File arquivo;
    
    public Utilitario(File arquivo) {
        this.arquivo = arquivo;
    }
    
    public int GetQtdDeLinhas() {
        int qtd_de_linhas = 0;
        
        try {
            Scanner leitor = new Scanner(arquivo, StandardCharsets.UTF_8.name());
            
            while (leitor.hasNextLine()) {
                leitor.nextLine();
                qtd_de_linhas += 1;
            }
        } catch (IOException e) {
            System.out.println(e);
        }
        
        return qtd_de_linhas;
    }
    
    public String[] GetFiliais() {
        int qtdDeLinhas = GetQtdDeLinhas();
        String[] filiais_repetidas = new String[qtdDeLinhas];
        
        try {
            Scanner leitor = new Scanner(arquivo, StandardCharsets.UTF_8.name());
            
            for (int i = 0; i < qtdDeLinhas; i++) {
                String linhaAtual = leitor.nextLine();
                filiais_repetidas[i] = linhaAtual.substring(0, linhaAtual.indexOf(","));
            }
        } catch (IOException e) {
            System.out.println("Deu ruim: " + e);
        }

        // https://stackoverflow.com/questions/13796928/how-to-get-unique-values-from-array
        String[] filiais_unicas = Arrays.stream(filiais_repetidas).distinct().toArray(String[]::new);
        
        return filiais_unicas;
    }
    
    public String[] GetLinhas() {
        String[] linhas = new String[GetQtdDeLinhas()];
        
        try {
            Scanner leitor = new Scanner(arquivo, StandardCharsets.UTF_8.name());
            
            int posicao = 0;
            while (leitor.hasNextLine()) {
                linhas[posicao] = leitor.nextLine();
                posicao++;
            }
        } catch (IOException e) {
            System.out.println(e);
        }
        
        return linhas;
    }
    
    public String[] GetVendasPorFilial() {
        String[] filiais = GetFiliais();
        String[] linhas = GetLinhas();
        String[] valoresCompletos = new String[GetFiliais().length];
        
        int cont = 0;
        for (String filial : filiais) {
            double media = 0, somatorio = 0, qtdParaMedia = 0;
            
            for (String linha : linhas) {
                if (filial.equals(linha.substring(0, linha.indexOf(",")))) {
                    somatorio += Double.parseDouble(linha.substring(linha.indexOf(",") + 1, linha.length()));
                    qtdParaMedia++;
                }
            }
            
            media = somatorio / qtdParaMedia;
            valoresCompletos[cont] = "A filial " + filial + " vendeu " + "R$ " + somatorio + " com uma média de R$ " + media;
            cont++;
        }
        
        return valoresCompletos;
    }
    
}
