/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package valordevendafiliais;

import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.lang.NullPointerException;
import java.io.IOException;

/**
 *
 * @author Deive
 */
public class SemVetor {
    
    private final String arquivoCaminho;
    
    public SemVetor(String arquivo) {
        this.arquivoCaminho = arquivo;
    }
    
    public void GetVendas() {
        
        try {
            FileReader fr = new FileReader(this.arquivoCaminho);
            BufferedReader br = new BufferedReader(fr);
            
            String linhaAtual = br.readLine();
            
            String filial = linhaAtual.substring(0, linhaAtual.indexOf(","));
            String filialAnterior = filial;
            double valorFilialAtual = Double.parseDouble(linhaAtual.substring(linhaAtual.indexOf(",") + 1, linhaAtual.length()));
            double qtdFilialAtual = 1;
            
            while (linhaAtual != null) {
                linhaAtual = br.readLine();
                
                filial = linhaAtual.substring(0, linhaAtual.indexOf(","));
                
                if (filialAnterior.equals(filial)) {
                    valorFilialAtual += Double.parseDouble(linhaAtual.substring(linhaAtual.indexOf(",") + 1, linhaAtual.length()));
                    qtdFilialAtual++;
                } else {
                    System.out.println("A filial " + filialAnterior + " vendeu R$ " + valorFilialAtual + " e sua média de venda é igual a R$ " + (valorFilialAtual / qtdFilialAtual) + "!!");
                    filialAnterior = filial;
                    valorFilialAtual = Double.parseDouble(linhaAtual.substring(linhaAtual.indexOf(",") + 1, linhaAtual.length()));
                    qtdFilialAtual = 1;
                }
            }
            
            br.close();
            fr.close();
        } catch (FileNotFoundException e) {
            System.out.println("O arquivo " + this.arquivoCaminho + " não foi encontrado!!\n" + e);
        } catch (NullPointerException e) {
            
        } catch (IOException e) {
            System.out.println("Erro na leitura do arquivo " + this.arquivoCaminho + "!!\n" + e);
        }
        
    }
    
}
