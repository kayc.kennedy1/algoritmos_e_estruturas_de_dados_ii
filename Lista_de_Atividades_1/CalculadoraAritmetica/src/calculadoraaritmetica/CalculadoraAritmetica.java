/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadoraaritmetica;
import java.util.Scanner;
/**
 *
 * @author Deive
 */
public class CalculadoraAritmetica {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        /*
        Faça um programa no qual o usuário possa escolher uma das opções:
        “1 - Multiplicação de inteiros”, “2 - Divisão de inteiros”, “3 - Adição de inteiros”, “4 -
        Subtração de inteiros” e “5 – Sair do programa”.
        De acordo com a escolha, ele poderá entrar com dois números.
        O resultado da operação matemática entre eles deverá ser exibido na tela e, logo em
        seguida, retornar ao menu principal.
        Para o caso de divisão por 0, apresentar a devida mensagem de erro para o usuário.
        Nome do programa: CalculadoraAritmetica.java
        */

        System.out.println("1 - Multiplicação de inteiros");
        System.out.println("2 - Divisão de inteiros");
        System.out.println("3 - Adição de inteiros");
        System.out.println("4 - Subtração de inteiros");
        System.out.println("5 - Sair do programa");

        Scanner meuTeclado = new Scanner(System.in);

        System.out.print("Escolha uma das opções acima: ");
        short numEscolhido = meuTeclado.nextShort();
        
        float resultado = 0;

        if (numEscolhido >= 1 && numEscolhido <= 4) {
            System.out.print("Insira o primero número: ");
            float numeroUm = meuTeclado.nextFloat();
            System.out.print("Insira o segundo número: ");
            float numeroDois = meuTeclado.nextFloat();

            switch (numEscolhido) {
                case 1:
                    resultado = numeroUm * numeroDois;
                break;
                case 2:
                    if (numeroDois != 0) {
                        resultado = numeroUm / numeroDois;
                    } else {
                        System.out.println("Impossível dividir por zero!!!");
                    }
                break;
                case 3:
                    resultado = numeroUm + numeroDois;
                break;
                case 4:
                    resultado = numeroUm - numeroDois;
                break;
                case 5:
                    System.out.println("Saindo...");
                break;
            }

            if (!(numEscolhido == 2 && numeroDois == 0)) {
                System.out.println("O resultado é igual a " + resultado);
            }
        } else if (numEscolhido == 5){
            System.out.println("Saindo...");
        } else {
            System.out.println("Escolha inválida...\nSaindo...");
        }

        meuTeclado.close();
    }
    
}
