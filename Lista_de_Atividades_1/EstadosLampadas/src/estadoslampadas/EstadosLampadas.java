/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estadoslampadas;

/**
 *
 * @author Deive
 */
public class EstadosLampadas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        /*
        Imagine uma lâmpada que possa ter três estados: apagada, acessa e meia-luz. Desenvolva
        um programa que instancie 3 lâmpadas. A primeira deverá ficar apagada, a segunda deverá
        ficar acessa e a terceira deverá ficar meia-luz.
        Dica: pode-se implementar o “estado” como um atributo String da classe. Na classe haverá
        três métodos: Acender(), Apagar() e AcenderMeiaLuz().
        */
        
        Lampada lampada_1 = new Lampada();
        System.out.println("O estado da lâmpada 1 é: " + lampada_1.GetEstado());
        
        Lampada lampada_2 = new Lampada();
        lampada_2.Acender();
        System.out.println("O estado da lâmpada 2 é: " + lampada_2.GetEstado());
        
        Lampada lampada_3 = new Lampada();
        lampada_3.AcenderMeiaLuz();
        System.out.println("O estado da lâmpada 3 é: " + lampada_3.GetEstado());
        
    }
    
}
