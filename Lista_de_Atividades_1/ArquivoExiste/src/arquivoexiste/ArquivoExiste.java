/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arquivoexiste;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.io.FileWriter;
import java.io.PrintWriter;

/**
 *
 * @author Deive
 */
public class ArquivoExiste {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        /*
        Escreva um programa em java que receba um nome de arquivo e uma sequência de
        palavras como argumentos na linha de comando, informe se o arquivo existe ou não, caso
        não exista, crie-o e, por fim, escreva neste arquivo a sequência de palavras passadas como
        argumentos.
        */
        
        Scanner teclado = new Scanner(System.in);
        
        System.out.print("Escreva o nome do arquivo: ");
        String arquivo = teclado.nextLine();
        
        System.out.print("Escreve a sequênci de palavras separadas por vírgula: ");
        String sequencia = teclado.nextLine();
        
        File arquivoCompleto = new File("src/arquivoexiste/" + arquivo);
        try {
            if (!arquivoCompleto.exists()) {
                System.out.println("O arquivo não existe.\nCriando neste momento...");
                arquivoCompleto.createNewFile();
            } else {
                System.out.println("O arquivo já existe...");
            }
            
            System.out.println("Gravando palavras no arquivo...");
            
            FileWriter arquivoWriter = new FileWriter(arquivoCompleto, true);
            PrintWriter arquivoPrint = new PrintWriter(arquivoWriter);
            
            String sequenciaCompleta = sequencia + ",";
            
            while (!sequenciaCompleta.equals("")) {
                String linhaTratada = sequenciaCompleta.substring(0, sequenciaCompleta.indexOf(","));
                arquivoPrint.println(linhaTratada);
                sequenciaCompleta = sequenciaCompleta.substring(sequenciaCompleta.indexOf(",") + 1, sequenciaCompleta.length());
            }
            
            arquivoWriter.close();
            
            System.out.println("Terminado...");
        } catch (IOException e) {
            System.out.println(e);
        }
        
    }
    
}
