import java.util.Scanner;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.FileWriter;

public class GravandoArquivo {

    public static void main (String[] args) {

        /*
        Apenas um teste para gravar aquivo txt usando java

        true para dar Append e não sobrescrever o arquivo
        */

        try {
            FileWriter arquivo = new FileWriter("GravandoArquivo.txt", true);
            PrintWriter gravarArquivo = new PrintWriter(arquivo);
            
            gravarArquivo.print("Isso é apenas um teste" + "\n");
            gravarArquivo.print("Mais um teste pulando linha talvez vamos ver");

            arquivo.close();
        } catch (Exception e) {
            System.out.println("Deu ruim: " + e);
        }

    }

}