/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg001primeiroprojeto;

/**
 *
 * @author Deive
 */
public class Soma {
    private short numero1 = 0;
    private short numero2 = 0;

    public Soma(short p_numero1, short p_numero2) {
        this.numero1 = p_numero1;
        this.numero2 = p_numero2;
    }
    
    public short somar() {
        short soma = (short) (this.numero1 + this.numero2);
        return soma;
    }
}
