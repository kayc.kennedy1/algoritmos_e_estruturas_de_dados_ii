/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg001primeiroprojeto;
import java.util.Scanner;
/**
 *
 * @author Deive
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {
        // TODO code application logic here
        
        System.out.println("Olá, mundo!!!");
        System.out.println("Apenas mais uma linha...");
        
        System.out.println("\nPulando para a\n\npróxima linha!!!");
        
        System.out.println("\nAgora usando tipo um TAB desse\n\tjeito!!");
        
        System.out.println("\nNão sei o que é esse \\r \"retorno\" de carro, vamos\n\rver");
        
        short num_1 = 12, num_2 = 13;
        System.out.println("\nA soma entre " + num_1 + " + " + num_2 + " é igual a " + (num_1 + num_2));
        
        
        // Usando classe criada e importada
        
        Soma somador;
        somador = new Soma((short) (10), (short) (5));
        System.out.println("A soma deu " + somador.somar() + "!!!");
        
        
        // Usando a classe criada junto com SCANNER
        Scanner meuScanner;
        meuScanner = new Scanner(System.in);
        
        
        System.out.print("\nInsira um número de baixo valor: ");
        short numeroUsuario1 = meuScanner.nextShort();
        
        System.out.print("Insira o segundo número de baixo valor: ");
        short numeroUsuario2 = meuScanner.nextShort();
        
        // Tenho que usar isso pra consumir o \n e não bugar o próximo meuScanner.nextLine();
        // https://stackoverflow.com/questions/13102045/scanner-is-skipping-nextline-after-using-next-or-nextfoo
        meuScanner.nextLine();
        
        Soma somatorio;
        somatorio = new Soma(numeroUsuario1, numeroUsuario2);
        
        System.out.println("A soma entre " + numeroUsuario1 + " e " + numeroUsuario2 + " é igual a " + somatorio.somar());
        
        
        // Usando a classe PESSOA
        Pessoa eu = new Pessoa("Teste 001", "123.456.789-01");
        System.out.println("\nNome: " + eu.getNome() + ". CPF: " + eu.getCpf() + "\n");
        
        // Usando métodos SET
        System.out.print("Insira seu nome: ");
        eu.setNome(meuScanner.nextLine());
        System.out.print("Insira seu CPF: ");
        eu.setCpf(meuScanner.nextLine());
        
        System.out.println("\nNome: " + eu.getNome() + ". CPF: " + eu.getCpf() + "\n");
    }
    
}
